#!/usr/bin/env python3
import math
import sys

def calculate_args(a, b, operator):
    print('Program arguments', sys.argv)


def calculate(a, b, operator):
    if operator == '+':
        result = a + b
    elif operator == '-':
        result = a - b
    elif operator == '*':
        result = a * b
    elif operator == '/':
        try:
            result = a / b
        except ZeroDivisionError:
            print('You cannot divide by zero!')
            result = 0
    elif operator == 'log':
        try:
            result = math.log(a) / math.log(b)
        except (ValueError, TypeError, ZeroDivisionError):
            print('Please double-check your values!')
            result = 0
        else:
            print('No errors!')
        finally:
            print('Finally!')
    return result


def run_script():
    print('Running as a script')
    for idx, arg in enumerate(sys.argv):
        print(f'arg {idx} is {arg}')

    if len(sys.argv) != 4:
        print('Usage: calculate_argv OPERAND OPERAND OPERATOR')
        sys.exit(-1)
    try:
        x = float(sys.argv[1])
        y = float(sys.argv[2])
        operator = sys.argv[3]
    except ValueError:
        print('Invalid parameter! Please supply numeric values')
    except IndexError:
        print('Usage: calculate_argv OPERAND OPERAND OPERATOR')
    else:
        print('Result:', calculate(x, y, operator))


if __name__ == '__main__':
    run_script()
else:
    print('Imported')
